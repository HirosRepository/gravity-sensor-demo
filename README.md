This android is created to test the gravity sensor feature. It takes the x and
y component of the gravitational force acting on the device and display it as
a 2D vector on the display. The color of the arrow reflects the strength 
of the gravity.


<img src="app/data/gravitySensorDemo.gif"  width="200" height="400">